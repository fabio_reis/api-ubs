package br.com.api.domain;

public class DetailErrors {
	
	private String title;
	private Long status;
	private Long timeStamp;
	private String message;
	
	public String getTitle() {return title;}
	public void setTitle(String title) {this.title = title;}
	
	public Long getStatus() {return status;}
	public void setStatus(Long status) {this.status = status;}
	
	public Long getTimeStamp() {return timeStamp;}
	public void setTimeStamp(Long timeStamp) {this.timeStamp = timeStamp;}
	
	public String getMessage() {return message;}
	public void setMessage(String message) {this.message = message;}	

}
