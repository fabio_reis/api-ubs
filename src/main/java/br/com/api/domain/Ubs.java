package br.com.api.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;


@Entity
public class Ubs implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private String name;
	private String address;
	private String city;	
	private String phone;
	private Double geocodeLat;
	private Double geocodeLong;
	private Integer scoresSize;
	private Integer scoresAdaptationForSeniors;
	private Integer socoresMedicalEquipment;
	private Integer scoresMedicine;	
	@Transient
	private Double distance;
	
	public Long getId() {return id;}
	public void setId(Long id) {this.id = id;}
	
	public String getName() {return name;}
	public void setName(String name) {this.name = name;}
	
	public String getAddress() {return address;}
	public void setAddress(String address) {this.address = address;}
	
	public String getCity() {return city;}
	public void setCity(String city) {this.city = city;}
	
	@JsonInclude(Include.NON_NULL)
	public String getPhone() {return phone;}
	public void setPhone(String phone) {this.phone = phone;}	
	
	public Double getGeocodeLat() {return geocodeLat;}
	public void setGeocodeLat(Double geocodeLat) {this.geocodeLat = geocodeLat;}
	
	public Double getGeocodeLong() {return geocodeLong;}
	public void setGeocodeLong(Double geocodeLong) {this.geocodeLong = geocodeLong;}
	
	public Integer getScoresSize() {return scoresSize;}
	public void setScoresSize(Integer scoresSize) {this.scoresSize = scoresSize;}
	
	public Integer getScoresAdaptationForSeniors() {return scoresAdaptationForSeniors;}
	public void setScoresAdaptationForSeniors(Integer scoresAdaptationForSeniors) {this.scoresAdaptationForSeniors = scoresAdaptationForSeniors;}
	
	public Integer getSocoresMedicalEquipment() {return socoresMedicalEquipment;}
	public void setSocoresMedicalEquipment(Integer socoresMedicalEquipment) {this.socoresMedicalEquipment = socoresMedicalEquipment;}
	
	public Integer getScoresMedicine() {return scoresMedicine;}
	public void setScoresMedicine(Integer scoresMedicine) {this.scoresMedicine = scoresMedicine;}	
	
	public Double getDistance() {return distance;}
	public void setDistance(Double distance) {this.distance = distance;}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ubs other = (Ubs) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
