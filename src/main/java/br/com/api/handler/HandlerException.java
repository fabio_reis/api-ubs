package br.com.api.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import br.com.api.domain.DetailErrors;

@RestControllerAdvice
public class HandlerException {
	
	@ExceptionHandler
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseEntity<?> handler(MissingServletRequestParameterException e) {
		
		DetailErrors errors = new DetailErrors();
		errors.setTitle("Error");
		errors.setStatus(400L);
		errors.setMessage("Requisição inválida: cheque se enviou os parâmetros geocodeLat e geocodeLong, "
				+ "Documentação: http://localhost:8080/swagger-ui.html");		
		errors.setTimeStamp(System.currentTimeMillis());
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);
		
	}

}
