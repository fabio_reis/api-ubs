package br.com.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.api.domain.Ubs;

@Repository
public interface UbsRepository extends JpaRepository<Ubs, Long>{

}
