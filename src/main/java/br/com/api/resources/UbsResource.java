package br.com.api.resources;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.api.services.UbsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController()
@RequestMapping(value="/api")
@Api(value="API REST UBS")
@CrossOrigin(origins="*")
public class UbsResource {
	
	@Autowired
	private UbsService service;
	
	@GetMapping("/ubs")
	@ApiOperation(value="Retorna uma lista com as 10 UBSs mais próximas, requer a longitude e latitude")
	public ResponseEntity<?> getUbs(@RequestParam Double geocodeLat,@RequestParam Double geocodeLong) {	
		
		//cache de 30 segundos 
		CacheControl cache = CacheControl.maxAge(30,TimeUnit.SECONDS);		
		
		return ResponseEntity.status(HttpStatus.OK).cacheControl(cache).body(service.findTenUBSs(geocodeLat,geocodeLong));	
		
	}
	
}
