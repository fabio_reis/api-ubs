package br.com.api.services;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.api.domain.Ubs;
import br.com.api.repository.UbsRepository;

//classe responsável por processar dados e entregar para os resources da api.
@Service
public class UbsService {
	
	//Raio da terra em 6371Km.
	public static int EARTH_RADIUS_KM = 6371;	
	
	@Autowired
	private UbsRepository repository;
	
	public static double getDistance(double firstLat, double firstLong, double secondLat,double secondLong) {		
		
		double firstLatRadians = Math.toRadians(firstLat);
		double secondLatRadians = Math.toRadians(secondLat);		
		
		double deltaLong = Math.toRadians(secondLong  - firstLong); 
		
		//Retorna a distancia entre as coordenadas
		return (Math.acos(Math.cos(firstLatRadians) * Math.cos(secondLatRadians)
				* Math.cos(deltaLong) + Math.sin(firstLatRadians)
				* Math.sin(secondLatRadians))
				* EARTH_RADIUS_KM);	
		}
	
	//retornar os 
	public List<Ubs> findTenUBSs(Double geocodeLat, Double geocodeLong){			
		
		List<Ubs> ubsList =  repository.findAll();	
		
		ubsList.forEach(ubs -> {
			
			Double currentDistance = getDistance(geocodeLat, geocodeLong,ubs.getGeocodeLat(), ubs.getGeocodeLong());
			ubs.setDistance(currentDistance);
			
		});		
		
		ubsList.sort(Comparator.comparing(Ubs::getDistance));	
		
		return ubsList
				.stream()
				.limit(10)
				.collect(Collectors.toList());
				
	}
	
}
