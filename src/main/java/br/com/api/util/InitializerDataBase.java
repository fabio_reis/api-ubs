package br.com.api.util;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.api.domain.Ubs;
import br.com.api.repository.UbsRepository;

//essa classe inicializa o banco de dados H2 e inseri os dados
@Component
public class InitializerDataBase {
	
	@Autowired
	private UbsRepository repository;
	
	@Autowired
	private UbsUtil ubsUtil;
	
	@PostConstruct
	public void insertDataBase() {
		
		List<Ubs> ubsList = ubsUtil.getUbsList();
		
		ubsList.forEach(ubs -> repository.save(ubs));		
		
	}

}
