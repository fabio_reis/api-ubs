package br.com.api.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import br.com.api.domain.Ubs;

@Component
public class UbsUtil {
	
	//faz a leitura do arquivo csv com os dados da UBS e retornar uma lista de UBSs
	private List<Ubs> readFile() {		
		
		List<Ubs> ubsList = new ArrayList<Ubs>();
		
		//busca o arquivo apartir do classPath(dentro da pasta resources)
		InputStream in = UbsUtil.class.getResourceAsStream("/ubs.csv");		
						
		InputStreamReader streamReader = new InputStreamReader(in, Charset.forName("UTF-8"));		
		
		try(BufferedReader reader = new BufferedReader(streamReader)){
			
			String line = null;
			
			while((line = reader.readLine()) != null) {				
								
				String[] lineTemp = line.split(",");			
					
				Ubs ubs = new Ubs();
				
				ubs.setGeocodeLat(Double.parseDouble(lineTemp[0]));
				ubs.setGeocodeLong(Double.parseDouble(lineTemp[1]));
				ubs.setId(Long.parseLong(lineTemp[2]));
				ubs.setName(lineTemp[4]);
				ubs.setAddress(lineTemp[5]);
				ubs.setCity(lineTemp[7]);
				
				if(!lineTemp[8].equals("Não se aplica"))
					ubs.setPhone(formatPhone(lineTemp[8]));									
				
				if(lineTemp[9].equals("Desempenho muito acima da média"))
					ubs.setScoresSize(3);
				else if(lineTemp[9].equals("Desempenho acima da média"))
					ubs.setScoresSize(2);
				else if(lineTemp[9].equals("Desempenho mediano ou  um pouco abaixo da média"))
					ubs.setScoresSize(1);
				
				if(lineTemp[10].equals("Desempenho muito acima da média"))
					ubs.setScoresAdaptationForSeniors(3);
				else if(lineTemp[10].equals("Desempenho acima da média"))
					ubs.setScoresAdaptationForSeniors(2);
				else if(lineTemp[10].equals("Desempenho mediano ou  um pouco abaixo da média"))
					ubs.setScoresAdaptationForSeniors(1);
				
				if(lineTemp[11].equals("Desempenho muito acima da média"))
					ubs.setSocoresMedicalEquipment(3);
				else if(lineTemp[11].equals("Desempenho acima da média"))
					ubs.setSocoresMedicalEquipment(2);
				else if(lineTemp[11].equals("Desempenho mediano ou  um pouco abaixo da média"))
					ubs.setSocoresMedicalEquipment(1);
				
				if(lineTemp[12].equals("Desempenho muito acima da média"))
					ubs.setScoresMedicine(3);
				else if(lineTemp[12].equals("Desempenho acima da média"))
					ubs.setScoresMedicine(2);
				else if(lineTemp[12].equals("Desempenho mediano ou  um pouco abaixo da média"))
					ubs.setScoresMedicine(1);
				
				ubsList.add(ubs);					
								
			}
			
			return ubsList;
			
		}catch(IOException e) {			
			
			throw new RuntimeException("Erro ao fazer a leitura do arquivo");
			
		}
		
	}	
	
	//formata o número de telefone
	private String formatPhone(String phone) {		
		return phone = phone.replaceAll("\\s","").replaceAll("\\D", "");	
	}
	
	public List<Ubs> getUbsList(){			
		return this.readFile();
	}
}
