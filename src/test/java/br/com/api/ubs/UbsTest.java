package br.com.api.ubs;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.api.services.UbsService;
import br.com.api.util.UbsUtil;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UbsTest {
	
	@Autowired
	private UbsService service;
	
	@Autowired
	private UbsUtil ubsUtil;	
	
	//teste de distância aproximada entre Sorocaba e São Paulo
	@Test
	public void testCalcDistance() {
		
		double distance = service.getDistance(-11.531371, -46.559372, -11.686069, -46.305313);
		
		assertThat(distance).isEqualTo(32.58291882849377);			
		
	}
	
	//verifica a quantidade de UBSs
	@Test
	public void testGetQuantityUbs() {
		
		Integer quantityUbs = ubsUtil.getUbsList().size();		
		
		assertThat(quantityUbs).isEqualTo(37690);			
		
	}	

}
